defmodule ApiWeb.Router do
  use ApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ApiWeb do
    pipe_through :api

    post "/model", ModelController, :create
    post "/model/dataset", ModelController, :create_model_dataset
    post "/job", JobController, :create
    get  "/job/:id", JobController, :status
    get  "/job/result/:id", JobController, :results
  end
end
