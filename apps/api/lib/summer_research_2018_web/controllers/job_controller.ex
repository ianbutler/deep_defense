defmodule ApiWeb.JobController do
  use ApiWeb, :controller
  alias Api.Defenses

  def create(conn, %{"type" => type, "model_id" => model_id, "dataset_id" => dataset_id}) do
    res = case type do
      "xplore" -> Defenses.start_job(:xplore, model_id, dataset_id)
      _        -> nil
    end

    errors = res[:errors]

    case errors = [] do
      :ok -> render(conn, "create_job.json", status: 200, job: res)
      _   -> render(conn, "error.json", status: 500, error_response: res)
    end
  end

  def status(conn, _) do
    job_id = conn.path_params["id"]

    job = Defenses.get_job_status(:xplore, job_id)

    errors = job[:errors]

    case errors = [] do
      :ok -> render(conn, "job_status.json", status: 200, job: job)
      _ -> render(conn, "error.json", status: 500, error_response: job)
    end
  end

  def results(conn, _) do
    job_id = conn.path_params["id"]

    %{ status: status, response: response } = Defenses.get_job_results(job_id)

    case status do
      200 -> render(conn, "result.json", status: status, result: response)
      _ ->
        conn
        |> put_status(response[:status])
        |> render("error.json", error_response: response)
    end
  end

end
