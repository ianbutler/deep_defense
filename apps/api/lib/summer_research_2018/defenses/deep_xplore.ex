defmodule Api.Defenses.DeepXplore do
  alias DefenseServices.XploreMonitor
  require JobProtocols
  require UUID

  @moduledoc """
  The DeepXplore sub-context. Api concerns relating to the corresponding job in the definse services app.
  """

  @doc """
    Start an xplore job.
  """
  def start_job(model_id, dataset_id, aws_creds = %{}) do
    job = JobProtocols.spawn_job(define_job(UUID.uuid4(), model_id, dataset_id), aws_creds)

    Map.from_struct(job)
  end

  @doc """
    Get the status of an existing job.
  """
  def get_job_status(job_id) do
    JobProtocols.get_job_status(define_job(job_id, nil, nil))
  end

  @doc """
    Get the results of a finsihed job.
  """
  def get_job_results(job_id, model_id) do
    XploreMonitor.get_job_results(define_job(job_id, model_id, nil))
  end

  defp define_job(job_id, model_id, dataset_id) do
    %XploreMonitor{ job_id: job_id, model_id: model_id, dataset_id: dataset_id, status: "STARTING" }
  end

end
