defmodule DefenseServices do
  @moduledoc """
  Documentation for DefenseServices.
  """

  @doc """
  Hello world.

  ## Examples

      iex> DefenseServices.hello
      :world

  """
  def hello do
    :world
  end
end
